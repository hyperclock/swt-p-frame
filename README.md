# Spacewax Technologies PHP Framework

**SWT P Frame** is a php framework built from scratch.

The project will be using the MVC pattern, includes the use of PDO and other goodies are planned.


## License
MIT - SEE [LICENSE](LICENSE) for details

```
Copyright (c) 2017-2018 hyperclock (hyperclock@spacewax.net)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
```

## Documentation
Non yet, though there may be some dev stuff in the [WIKI](wikis/home)
