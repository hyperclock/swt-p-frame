<?php
/*
Copyright (c) 2017-2018 hyperclock (hyperclock@spacewax.net)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
*/

// DB PARAMS
define('DB_HOST', 'localhost');
define('DB_USER', 'hyperclock');
define('DB_PASS', 'feder04991');
define('DB_NAME', 'spx_p_frame_db1');

// APP ROOT
define('APPROOT', dirname(dirname(__FILE__)));

// URL ROOT
define('URLROOT', 'http://localhost/swt-p-frame');

// SITE NAME
define('SITENAME', 'Spacewax Frame');
