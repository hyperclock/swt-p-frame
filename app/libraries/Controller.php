<?php
/*
Copyright (c) 2017-2018 hyperclock (hyperclock@spacewax.net)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
*/

/**
  * Base Controller
  * Loads models and views
  */
  class Controller
  {
      // LOAD MODEL
      public function model($model)
      {
          // REQUIRE MODEL
          require_once '../app/models/' . $model . '.php';

          // INSTANTIATE MODEL
          return new $model();
      }

      // LOAD VIEW
      public function view($view, $data = [])
      {
          // CHECK FOR VIEW FILE
          if (file_exists('../app/views/' . $view . '.php')) {
              require_once '../app/views/' . $view . '.php';
          } else {
              die('View does not exist.');
          }
      }
  }
