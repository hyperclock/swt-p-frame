<?php
/*
Copyright (c) 2017-2018 hyperclock (hyperclock@spacewax.net)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
*/

class Pages extends Controller
{
    public function __construct()
    {
      $this->postModel = $this->model('Post');
    }

    public function index()
    {
      $posts = $this->postModel->getPosts();

        $data =
        [
          'title' => 'Welcome',
          'posts' => $posts
        ];

        $this->view('pages/index', $data);
    }

    public function about()
    {
      $data =
      [
        'title' => 'About Us'
      ];

        $this->view('pages/about', $data);
    }
}
